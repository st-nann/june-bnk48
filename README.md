# june-bnk48

## ENV BUILD

```bash
MODE                            string      build mode
NODE_ENV                        string      environment build
DEBUG                           boolean     debug log
TITLE                           string      title website
FAVICON_NAME                    string      favicon\'s location name on firebase storage
LOGO_TRANSPARENT_NAME           string      logo\'s location name on firebase storage (transparent)
LOGO_CREATOR_NAME               string      logo\'s location name on firebase storage (creator)
```

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Run your tests

```
yarn run test
```

### Lints and fixes files

```
yarn run lint
```

### Run your end-to-end tests

```
yarn run test:e2e
```

### Run your unit tests

```
yarn run test:unit
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
