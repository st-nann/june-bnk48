module.exports = {
  home: {
    desktop: {
      name: 'home%2Fhome-desktop'
    },
    tablet: {
      name: 'home%2Fhome-tablet'
    },
    mobile: {
      name: 'home%2Fhome-mobile'
    }
  }
}
