module.exports = {
  cover: {
    name: 'performance%2Fcover'
  },
  category: [
    {
      name: 'Single',
      image: {
        name: 'performance%2Fsingle'
      },
      key: 'single'
    },
    {
      name: 'Official',
      image: {
        name: 'performance%2Fofficial'
      },
      key: 'official'
    },
    {
      name: 'Digital Live Studio (ตู้ปลา)',
      image: {
        name: 'performance%2Fdigitallivestudio'
      },
      key: 'digital_live_studio'
    },
    {
      name: 'Live',
      image: {
        name: 'performance%2Flive'
      },
      key: 'live'
    },
    {
      name: 'Music Video (MV)',
      image: {
        name: 'performance%2Fmv'
      },
      key: 'music_video'
    },
    {
      name: 'Acting',
      image: {
        name: 'performance%2Fserie'
      },
      key: 'acting'
    },
    {
      name: 'Presenter',
      image: {
        name: 'performance%2Fpresenter'
      },
      key: 'presenter'
    },
    {
      name: 'Other',
      image: {
        name: 'performance%2Fother'
      },
      key: 'other'
    }
  ]
}
