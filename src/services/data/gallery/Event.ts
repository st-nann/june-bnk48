module.exports = {
  event: {
    ['18052019_money_expo']: [
      { credit: 'mukashi1911' },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo001'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo002'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo003'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo004'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo005'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo006'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo007'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo008'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo009'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo010'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo011'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo012'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo013'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo014'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo015'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo016'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo017'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo018'
        }
      },
      {
        name: 'Money Expo 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.05.18%20Money%20Expo%202019%2Fmoneyexpo019'
        }
      }
    ],
    ['23012019_teaw_mueng_thai']: [
      { credit: 'mukashi1911' },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai001'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai002'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai003'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai004'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai005'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai006'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai007'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai008'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai009'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai010'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai011'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai012'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai013'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai014'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai015'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai016'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai017'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai018'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai019'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai020'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai021'
        }
      },
      {
        name: 'เที่ยวเมืองไทย',
        image: {
          name:
            'gallery%2Fevent%2F2019.01.23%20เทศกาลเที่ยวเมืองไทย%20%40สวนลุม%2Fteawmuangthai022'
        }
      }
    ],
    ['0602019_yaowarat']: [
      { credit: 'mukashi1911' },
      {
        name: 'ตรุษจีนเยาวราช 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.02.06%20เทศกาลตรุษจีนเยาวราช%2Fyaowarat001'
        }
      },
      {
        name: 'ตรุษจีนเยาวราช 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.02.06%20เทศกาลตรุษจีนเยาวราช%2Fyaowarat002'
        }
      },
      {
        name: 'ตรุษจีนเยาวราช 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.02.06%20เทศกาลตรุษจีนเยาวราช%2Fyaowarat003'
        }
      },
      {
        name: 'ตรุษจีนเยาวราช 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.02.06%20เทศกาลตรุษจีนเยาวราช%2Fyaowarat004'
        }
      },
      {
        name: 'ตรุษจีนเยาวราช 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.02.06%20เทศกาลตรุษจีนเยาวราช%2Fyaowarat005'
        }
      },
      {
        name: 'ตรุษจีนเยาวราช 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.02.06%20เทศกาลตรุษจีนเยาวราช%2Fyaowarat006'
        }
      },
      {
        name: 'ตรุษจีนเยาวราช 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.02.06%20เทศกาลตรุษจีนเยาวราช%2Fyaowarat007'
        }
      },
      {
        name: 'ตรุษจีนเยาวราช 2019',
        image: {
          name:
            'gallery%2Fevent%2F2019.02.06%20เทศกาลตรุษจีนเยาวราช%2Fyaowarat008'
        }
      }
    ],
    ['27032019_mirinda']: [
      { credit: 'mukashi1911' },
      {
        name: 'Mirinda Mix-It',
        image: {
          name: 'gallery%2Fevent%2F2019.03.27%20Mirinda%20mix-it%2Fmirinda001'
        }
      },
      {
        name: 'Mirinda Mix-It',
        image: {
          name: 'gallery%2Fevent%2F2019.03.27%20Mirinda%20mix-it%2Fmirinda002'
        }
      },
      {
        name: 'Mirinda Mix-It',
        image: {
          name: 'gallery%2Fevent%2F2019.03.27%20Mirinda%20mix-it%2Fmirinda003'
        }
      },
      {
        name: 'Mirinda Mix-It',
        image: {
          name: 'gallery%2Fevent%2F2019.03.27%20Mirinda%20mix-it%2Fmirinda004'
        }
      }
    ],
    ['03042019_tinten_fan_meet']: [
      { credit: 'mukashi1911' },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten001'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten002'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten003'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten004'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten005'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten006'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten007'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten008'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten009'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten010'
        }
      },
      {
        name: 'TINTEN x BNK48 Fanmeet',
        image: {
          name:
            'gallery%2Fevent%2F2019.04.03%20Fan%20meet%20TinTen%20x%20BNK48%2Ftinten011'
        }
      }
    ]
  }
}
