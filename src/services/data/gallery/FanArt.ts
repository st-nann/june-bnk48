module.exports = {
  fan_art: [
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-21'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-14'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-18'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-16'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-11'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-12'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-9'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-1'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-10'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-13'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-15'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-17'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-2'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-19'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-20'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-8'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-22'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-6'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-5'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-23'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-3'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-4'
      }
    },
    {
      name: 'fanart.bnk48 (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ffanart.bnk48-7'
      }
    },
    {
      name: 'tor.sketch (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Ftor.sketch%20(IG)'
      }
    },
    {
      name: 'rotdeepduck (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Frotdeepduck-1'
      }
    },
    {
      name: 'Nori Studio (facebook)',
      image: {
        name: 'gallery%2Ffanart%2Fnoristudio'
      }
    },
    {
      name: 'juné line square',
      image: {
        name: 'gallery%2Ffanart%2FjuneSQ-1'
      }
    },
    {
      name: 'juné family',
      image: {
        name: 'gallery%2Ffanart%2Fjunefamily-1'
      }
    },
    {
      name: 'juné family',
      image: {
        name: 'gallery%2Ffanart%2Fjunefamily-2'
      }
    },
    {
      name: 'Fluck Siripong Srianan (facebook)',
      image: {
        name: 'gallery%2Ffanart%2FFluck%20Siripong%20Srianan%20(FACEBOOK)'
      }
    },
    {
      name: 'armmfoo (instagram)',
      image: {
        name: 'gallery%2Ffanart%2Farmmfoo%20(IG)'
      }
    },
    {
      name: 'juné line square',
      image: {
        name: 'gallery%2Ffanart%2FjuneSQ-2'
      }
    },
    {
      name: 'juné line square',
      image: {
        name: 'gallery%2Ffanart%2FjuneSQ-3'
      }
    },
    {
      name: 'ธเนศพล กมลชัยวานิช',
      image: {
        name: 'gallery%2Ffanart%2Fธเนศพล%20กมลชัยวานิช-1'
      }
    },
    {
      name: 'ธเนศพล กมลชัยวานิช',
      image: {
        name: 'gallery%2Ffanart%2Fธเนศพล%20กมลชัยวานิช-2'
      }
    }
  ]
}
