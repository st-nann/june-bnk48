module.exports = {
  title: {
    image: {
      mobile: {
        name: 'profile%2Factivity%2Ftitle%2Ftitle%20(mobile)'
      },
      desktop: {
        name: 'profile%2Factivity%2Ftitle%2Ftitle%20(desktop)'
      }
    }
  },
  activity: [
    {
      name: 'Juné exhibition',
      cover: {
        name: 'profile%2Factivity%2Fexhibition'
      },
      owner: 'JUst NÉ - Juné BNK48',
      detail: {
        description: 'Birth day 2020',
        link:
          'https://artspaces.kunstmatrix.com/en/exhibition/1452197/just-an-extraordinary-girl',
        period: {
          start: 1593820800,
          expiry: 1596499200
        },
        image: [
          { name: 'profile%2Factivity%2Fexhibition2' },
          { name: 'profile%2Factivity%2Fexhibition' }
        ]
      }
    },
    {
      name: 'Vote for Juné',
      cover: {
        name: 'profile%2Factivity%2Fvoteforjune'
      },
      owner: 'Mukashi1911',
      detail: {
        description: 'General Election (Round 2)',
        link: 'https://www.facebook.com/voteforjune48/',
        period: {
          start: 1580515200,
          expiry: 1584835200
        },
        image: [{ name: 'profile%2Factivity%2Fvoteforjune' }]
      }
    },
    {
      name: 'Birthday 19th Juné Project',
      cover: {
        name: 'profile%2Factivity%2Fbirthday-project'
      },
      owner: 'Mukashi1911',
      detail: {
        description:
          'ขอเชิญ #เหล่าสมุนบะหมี่ ร่วม Donate เพื่อสร้างโปรเจควันเกิดให้กับ Juné (ขั้นต่ำ 50) ทุกการ Donate จะได้รับของที่ระลึก หากสนใจสามารถ Donate และกรอกแบบฟอร์มด้านล่าง(รายละเอียด สามารถดูได้ในฟอร์มค่ะ)',
        link: 'https://forms.gle/v32KXzhNjsXCioER6',
        period: {
          start: 1554681600,
          expiry: 1562198400
        },
        image: [{ name: 'profile%2Factivity%2Fbirthday-project' }]
      }
    },
    {
      name: 'Birthday 19th Juné Project',
      cover: {
        name: 'profile%2Factivity%2Factivity2'
      },
      owner: 'JUNÉonly',
      detail: {
        description: `มาร่วมเป็นส่วนหนึ่งในโปรเจคส่งต่อของขวัญวันเกิดให้จูเน่กันเถอะ! แค่ Donate เข้าโปรเจค H4PP7 JU DAY หรือ IT'S ALL GONNA HAPPEN TO JU ตั้งแต่ 70 บาทขึ้นไป และกรอกแบบฟอร์มรับของที่ระลึกไปเลย ดูรายละเอียดเพิ่มเติมได้ในฟอร์มจ้า`,
        link: 'https://t.co/nmafRseiHN',
        period: {
          start: 1557795600,
          expiry: 1559692800
        },
        image: [{ name: 'profile%2Factivity%2Factivity2' }]
      }
    },
    {
      name: 'Birthday 19th Juné Project',
      cover: {
        name: 'profile%2Factivity%2Fbirthday(ju%20fam)%20-%201'
      },
      owner: 'JUNÉ Family',
      detail: {
        description: `ขอเชิญแฟนคลับและคนที่รัก จูเน่ BNK48 ทุกท่าน ร่วมส่งต่อความสุขเนื่อง ในวันเกิดของจูเน่ ร่วมเลี้ยงอาหารคนชรา และมอบของใช้ที่จำเป็น ณ ศพส.บ้านบางแค วันเสาร์ที่ 8 มิถุนายน 2562 ตั้งแต่เวลา 10.30-12.30 สามารถดูรายละเอียดเพิ่มเติมได้ที่ `,
        link: 'https://www.facebook.com/337495687038962/posts/438078240314039/',
        period: {
          start: 1559989800,
          expiry: 1559997000
        },
        image: [
          { name: 'profile%2Factivity%2Fbirthday(ju%20fam)%20-%201' },
          { name: 'profile%2Factivity%2Fbirthday(ju%20fam)%20-%202' },
          { name: 'profile%2Factivity%2Fbirthday(ju%20fam)%20-%203' },
          { name: 'profile%2Factivity%2Fbirthday(ju%20fam)%20-%204' }
        ]
      }
    }
  ]
}
